from typing import Iterable, Awaitable
from gcloud.aio.storage import Storage
from gcloud.aio.auth import Token
from asyncio import gather


async def inmem_gcs_async_bulk(sources: Iterable) -> Awaitable:
    """
    async bulk ***in memory***
    download from gcs

    Usage:
    from asyncio import run
    in_mem_files = run(inmem_gcs_async_bulk(sources=gcs_path_list))
    """

    async with Storage(token=Token()) as client:
        tasks = (
            client.download(source.split("/", 1)[0], source.split("/", 1)[1])
            for source in sources
        )
        return await gather(*tasks)
