from typing import Any


class nonedict(dict):
    """
    Helper DS that doesnt raise
    KeyError upon missing key access.
    """

    def __missing__(self, key) -> None:
        return None


class Tracker:
    """
    Tracker that can be reset;
    keeps instance-level state.
    """

    def __init__(self, val: Any) -> None:
        self.__start = self.__state = val

    def __call__(self, val: Any = None) -> Any:
        if val is not None and self.__state != val:
            self.__state = val
        return self.__state

    def reset(self) -> Any:
        self.__state = self.__start
        return self.__state

class NestedAttrs:
    """
    Helper class to create nested obj
    to replicate nested dicts
    """

    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            if isinstance(val, (dict, nonedict)) and val:
                val = NestedAttrs(**val)
            setattr(self, key, val)