from itertools import compress
from typing import Iterable

def by_lookup_attr(sources: Iterable, lookup: dict) -> Iterable:
    """
    Index and extract results
    in order of lookup keys
    
    Usage:
    model_meta_df_iter, processed_traces_df_iter = by_lookup_attr(
        sources=sources_df_list,
        lookup={
            "model_meta_df": "MODEL_FILE_LOCATION",
            "in_mem_files": "TIME_SEQUENCE",
        },
    )
    """
    return map(
        lambda filtr: compress(sources, filtr),
        map(
            lambda key: list(map(lambda source: lookup[key] in source, sources)), lookup
        ),
    )
