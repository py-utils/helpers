from typing import List
from types import FunctionType
from functools import reduce

def func_chain(func_list: List[FunctionType]) -> FunctionType:
    """
    Single input argument.
    Order must be from right to left.
    f◦g(x) <= f(g(x)) <= [f,g].

    Usage:
    convolved_process = convolve(
        [
            chain.from_iterable,
            json.loads
        ]
    )
    """
    return reduce(lambda f, g: lambda x: f(g(x)), func_list, lambda x: x)