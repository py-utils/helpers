from flask_executor.executor import Executor
from opentelemetry.sdk.trace import Tracer
from opentelemetry.trace import get_tracer
from opentelemetry.context.context import Context
from contextlib import nullcontext
from wrapt import decorator
from flask import current_app
from functools import partial

class OTelExecutor(Executor):
    """
    Override and prepare/decorate
    """

    def __init__(self, app=None, name="", otel_ctx: Context = nullcontext):
        super().__init__(app, name)
        self.otel_ctx = otel_ctx
        self.tracer = get_tracer(__name__)

    def init_app(self, app):
        return super().init_app(app)

    def _make_executor(self, app):
        return super()._make_executor(app)
    
    def submit(self, fn, *args, **kwargs):
        fn = inctx_spanwrap(self.tracer, self.otel_ctx)(fn)
        return super().submit(fn, *args, **kwargs)
    
    def map(self, fn, *iterables, **kwargs):
        fn = inctx_spanwrap(self.tracer, self.otel_ctx)(fn)
        return super().map(fn, *iterables, **kwargs)
    
    def submit_stored(self, future_key, fn, *args, **kwargs):
        return super().submit_stored(future_key, fn, *args, **kwargs)
    
    def job(self, fn):
        return super().job(fn)
    
    def add_default_done_callback(self, fn):
        return super().add_default_done_callback(fn)

    def _prepare_fn(self, fn, force_copy=False):
        return super()._prepare_fn(fn, force_copy)

def inctx_spanwrap(tracer: Tracer, otel_ctx: Context = nullcontext):
    """
    Decorator to span a function
    """

    @decorator
    def wrapper(wrapped, instance, args, kwargs):
        # If tracing is enabled
        if current_app.config["ENABLE_TRACING"]:
            with tracer.start_as_current_span(
                (
                    wrapped.func.__name__
                    if isinstance(wrapped, partial)
                    else wrapped.__name__
                ),
                context=otel_ctx,
            ):
                return wrapped(*args, **kwargs)
        else:
            return wrapped(*args, **kwargs)

    return wrapper

##### __init__.py #####
# from warnings import filterwarnings
# from flask import Flask, g
# import src.services.settings as settings
# from src.config import Config, ProdConfig
# from src.views import (
#     golden_band_view,
#     hello_view,
#     process_multiple_raw_traces_to_dtw_view,
#     process_raw_trace_to_dtw_view,
# )
# from opentelemetry import trace
# from opentelemetry.sdk.trace import TracerProvider
# from opentelemetry.exporter.cloud_trace import CloudTraceSpanExporter
# from opentelemetry.sdk.trace.export import BatchSpanProcessor
# from google.cloud.bigquery import opentelemetry_tracing

# # Ignore deprecation and future
# filterwarnings("ignore", category=DeprecationWarning)
# filterwarnings("ignore", category=FutureWarning)

# def before_request():
#     """
#     Register when needed
#     """
#     g.timings = {}


# def after_request(response):
#     """
#     Register when needed
#     """
#     print(str(g.timings).encode("ascii"))
#     return response


# def create_app(config: Config = ProdConfig) -> Flask:
#     """
#     App Factory
#     #TODO: Toggle Instrumentation on/off - Conditional OTel threads
#     """

#     app = Flask(__name__)
#     app.config.from_object(config)

#     # Pass app context just in case
#     with app.app_context():

#         # Load settings
#         settings.init()
#         settings.lazy_loading()

#         # Register Routes
#         app.register_blueprint(hello_view.hl)
#         app.register_blueprint(golden_band_view.gb)
#         app.register_blueprint(process_raw_trace_to_dtw_view.rt)
#         app.register_blueprint(process_multiple_raw_traces_to_dtw_view.mrt)

#         # Register conditional callbacks
#         if app.config["ENABLE_BENCHMARKING"]:
#             app.before_request(before_request)
#             app.after_request(after_request)

#         # Configure OTel
#         if app.config["ENABLE_TRACING"]:
#             trace.set_tracer_provider(TracerProvider())
#             trace.get_tracer_provider().add_span_processor(BatchSpanProcessor(CloudTraceSpanExporter()))
#             # Disable default BQ instrumentation (MUST BE MODULE LEVEL)
#             opentelemetry_tracing.HAS_OPENTELEMETRY = False

#         return app

##### process_multiple_raw_traces_to_dtw_controller.py #####
# import logging
# import os, sys
# from numbers import Real
# from contextlib import nullcontext
# import pandas as pd
# import src.services.settings as settings
# import src.services.gcp_funcs as gcp_funcs
# from src.utils.helpers import prepare_propogated_ctx
# from google.auth.transport.requests import AuthorizedSession
# # from flask_executor import Executor
# from src.utils.helpers import OTelExecutor as Executor
# from flask import current_app, Response
# from concurrent.futures import as_completed
# from functools import partial
# from datetime import datetime, timedelta, timezone
# from gdw_fab_utils.utils import Fab
# from google.cloud import bigquery
# from typing import Iterable
# from src.utils.helpers import loose_apply_partial
# from time import time_ns
# from src.utils.decorations import timewrap, inctx_spanwrap
# from opentelemetry import trace
# from opentelemetry.trace.propagation.tracecontext import TraceContextTextMapPropagator
# from opentelemetry.context.context import Context

# FORMAT = "%(asctime)s %(levelname)-8s  [%(filename)s:%(lineno)d]  %(message)s"
# logging.basicConfig(format=FORMAT)

# authed_session = None


# def main(request_json, headers):

#     # Initialize tracer
#     tracer = trace.get_tracer(__name__) if current_app.config["ENABLE_TRACING"] else None

#     # Get propogated context
#     spanning_ctx = prepare_propogated_ctx(
#         title="process_multiple_raw_traces_to_dtw",
#         headers=headers,
#         tracer=tracer,
#         switch=current_app.config["ENABLE_TRACING"],
#     )

#     start = time_ns()

#     try:

#         with spanning_ctx:

#             # Get in-context span & traceparent
#             ctx = trace.set_span_in_context(
#                 trace.NonRecordingSpan(trace.get_current_span().get_span_context())
#             )

#             # get gcp token
#             global credentials
#             credentials = timewrap(inctx_spanwrap(tracer, ctx)(gcp_funcs.get_credentials))()

#             # # prepare list of query sources
#             sources = [
#                 *loose_apply_partial(
#                     [
#                         timewrap(source_runids_from_gdw_or_ytap),
#                         timewrap(infer_smartband_alg_version),
#                     ],
#                     **request_json,
#                 )
#             ]

#             # Fetch from sources
#             with Executor(app=current_app, otel_ctx=ctx) as executor:
#                 futures = [executor.submit(parfunc) for parfunc in sources]
#                 sources_df_list = [future.result() for future in as_completed(futures)]

#             # Prepare list of destinations
#             destinations = timewrap(inctx_spanwrap(tracer, ctx)(pre_processing))(
#                 sources_df_list=sources_df_list, request_json=request_json, ctx=ctx
#             )

#             # Fetch from destinations
#             with Executor(app=current_app, otel_ctx=ctx) as executor:
#                 futures = [executor.submit(parfunc) for parfunc in destinations]
#                 gdw_result_df = [future.result() for future in as_completed(futures)]

#             logging.info(f"API Execution time: {(time_ns() - start) // 10**9} sec")

#             # Send results in expected format
#             return timewrap(inctx_spanwrap(tracer, ctx)(post_processing))(gdw_result_df), 200

#     except Exception:

#         exc_typ, exc_obj, exc_tb = sys.exc_info()
#         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

#         message = (
#             f"{exc_typ.__doc__} -> {exc_typ.__name__}:{exc_obj} at {fname} line"
#             f" {exc_tb.tb_lineno}"
#         )

#         logging.error(f"Error in multiple_raw_traces_to_dtw: {message}")

#         logging.info(f"API Execution time: {(time_ns() - start) // 10**9} sec")

#         return Response(
#             status=500,
#             response=message,
#         )


# def send_http_request_multiple_gdw_runs(
#     input: pd.DataFrame, request_json: dict, context: Context
# ):

#     endpoint_name = "process_raw_trace_to_dtw"
#     body = {
#         "tool_id": request_json["tool_id"],
#         "tool_name": request_json["tool_name"],
#         "run_id_list": input["RUN_ID"].astype(str).tolist(),
#         "start_time_list": input["START_TIME"]
#         .dt.strftime("%Y-%m-%dT%H:%M:%S")
#         .tolist(),
#         "sensor_name": request_json["sensor_name"],
#         "fab_name": request_json["fab_name"],
#         "model_file_uuid": request_json["model_file_uuid"],
#         "trace_type": "multiple_gdw_runs",
#         "inference_algo_info": {
#             "ALGORITHM_NAME": "SmartBand",
#             "ALGORITHM_VERSION": f"{input.name[0]}",
#             # "ALGORITHM_VERSION": f"{input.name}",
#             "ALGORITHM_PATH": input["ALGORITHM_PATH"][input.index[0]],
#             "ALGO_MODULE_IMPORT_STR": input["ALGO_MODULE_IMPORT_STR"][input.index[0]],
#         },
#     }
#     # headers = {"Content-Type": "application/json"}
#     headers = {}

#     TraceContextTextMapPropagator().inject(headers, context)

#     with AuthorizedSession(credentials) as authed_session:
#         service_response = authed_session.request(
#             "POST",
#             url=settings.service_url + "/" + endpoint_name,
#             json=body,
#             timeout=360,
#             headers=headers,
#         )

#     return service_response.json()


# def send_http_request_ytap_runs(
#     input: pd.DataFrame, request_json: dict, context: Context
# ):

#     endpoint_name = "process_raw_trace_to_dtw"
#     body = {
#         "tool_id": request_json["tool_id"],
#         "tool_name": request_json["tool_name"],
#         "run_id_list": input["RUN_ID"].astype(str).tolist(),
#         "start_time_list": input["START_TIME"]
#         .dt.strftime("%Y-%m-%d %H:%M:%S")
#         .tolist(),
#         "sensor_name": request_json["sensor_name"],
#         "fab_name": request_json["fab_name"],
#         "model_file_uuid": request_json["model_file_uuid"],
#         "gcs_path_list": input["GCS_PATH"].astype(str).tolist(),
#         "trace_type": "multiple_ytap_runs",
#         "inference_algo_info": {
#             "ALGORITHM_NAME": "SmartBand",
#             "ALGORITHM_VERSION": f"{input.name[0]}",
#             # "ALGORITHM_VERSION": f"{input.name}",
#             "ALGORITHM_PATH": input["ALGORITHM_PATH"][input.index[0]],
#             "ALGO_MODULE_IMPORT_STR": input["ALGO_MODULE_IMPORT_STR"][input.index[0]],
#         },
#     }
#     # headers = {"Content-Type": "application/json"}
#     headers = {}

#     TraceContextTextMapPropagator().inject(headers, context)

#     with AuthorizedSession(credentials) as authed_session:
#         service_response = authed_session.request(
#             "POST",
#             url=settings.service_url + "/" + endpoint_name,
#             json=body,
#             timeout=360,
#             headers=headers,
#         )
#     return service_response.json()


# def source_runids_from_gdw_or_ytap(
#     fab_name: str,
#     start_time_list: list,
#     run_id_list,
#     tool_name: str,
#     tool_id: str,
#     sensor_name: str,
#     ytap_only: bool = False,
# ) -> tuple:
#     """
#     Thread-blocking BQ query task
#     to map run-id information
#     required from GDW or YTAP
#     """

#     run_id_list_integer = [int(x) for x in run_id_list]
#     try:
#         start_time_list_date = [
#             datetime.strptime(x, "%Y-%m-%d %H:%M:%S") for x in start_time_list
#         ]
#     except:
#         start_time_list_date = [
#             datetime.strptime(x, "%Y-%m-%dT%H:%M:%S") for x in start_time_list
#         ]
#     min_date = min(start_time_list_date)
#     max_date = max(start_time_list_date)
#     min_date_minus_24h = min_date - timedelta(days=1)

#     if not ytap_only:
#         # We need to keep this join to make sure data exists in gdw using rds2
#         query = """ 
#         select distinct cc.RUN_ID, cc.START_TIME
#         from `gdw-prod-data.{fab_name}_fd.fd_common_context` cc
#         join `gdw-prod-data.{fab_name}_fd.run_data_summary2` rds2 using (TOOL_ID, RUN_ID, START_TIME)
#         where DATE(cc.START_DATE) BETWEEN DATE('{min_date}') AND DATE('{max_date}')
#         and DATE(rds2.START_DATE) BETWEEN DATE('{min_date}') AND DATE('{max_date}')
#         AND cc.RUN_ID IN UNNEST ({run_id_list_integer})
#         AND TOOL_ID = {tool_id}
#         """.format(
#             fab_name=Fab(fab_name).get_fab_name("fab_"),
#             tool_id=tool_id,
#             tool_name=tool_name,
#             min_date=min_date,
#             max_date=max_date,
#             run_id_list_integer=run_id_list_integer,
#             min_date_minus_24h=min_date_minus_24h,
#             run_id_list=run_id_list,
#             database=settings.database,
#             sensor_name=sensor_name,
#         )

#         job_config = bigquery.QueryJobConfig(use_query_cache=settings.use_query_cache)
#         query_job = settings.bigquery_client.query(query, job_config=job_config)
#         logging.info(f"source_runids_from_gdw job-id: {query_job.job_id}")
#         gdw_df = (
#             query_job.to_arrow(bqstorage_client=settings.bigquery_storage_client)
#             .to_pandas()
#             .astype({"RUN_ID": "Int64", "START_TIME": "datetime64[ns]"})
#         )

#         # ASSUMPTION: No repetitions of run ids
#     else:
#         gdw_df = pd.DataFrame({"RUN_ID": [], "START_TIME": []}).astype(
#             {"RUN_ID": "Int64", "START_TIME": "datetime64[ns]"}
#         )

#     if len(run_id_list_integer) > gdw_df.shape[0]:

#         # ASSUMPTION: Using MAX of run start time from gdw "should" suffice
#         # to extract relevant partitions; ytap data will differ in
#         # only the latest runs OR all "earlier" runs ( < MAX of gdw run start time )
#         # will exist in both ( NO run information will be omitted from gdw )

#         min_date = max(
#             (
#                 gdw_df.START_TIME.max()
#                 .replace(tzinfo=Fab(fab_name).get_timezone())
#                 .astimezone(timezone.utc)
#                 if gdw_df.START_TIME.shape[0] > 0
#                 else min_date.replace(tzinfo=Fab(fab_name).get_timezone()).astimezone(
#                     timezone.utc
#                 )
#             ),
#             pd.Timestamp.now(tz=timezone.utc) - timedelta(days=30),
#         )
#         # CONVERT TO UTC TIME
#         ytap_run_ids = set(run_id_list_integer).difference(set(gdw_df.RUN_ID))

#         if len(ytap_run_ids) > 0:

#             query = """
#             select distinct RUN_ID, START_TIME, GCS_PATH
#             from `gdw-prod-data.{fab_name}_fd_trace.fd_rundata`
#             where TOOL_NAME = "{tool_name}"
#             AND RUN_ID IN UNNEST ({run_id_list})
#             AND (_PARTITIONTIME is null OR _PARTITIONTIME >= TIMESTAMP_TRUNC('{min_date_minus_24h}', HOUR)) --this is UTC time
#             """.format(
#                 fab_name=Fab(fab_name).get_fab_name("fab_"),
#                 tool_name=tool_name,
#                 min_date_minus_24h=min_date,
#                 run_id_list=[str(ytap_run_id) for ytap_run_id in ytap_run_ids],
#                 database=settings.database,
#             )

#             job_config = bigquery.QueryJobConfig(
#                 use_query_cache=settings.use_query_cache
#             )
#             query_job = settings.bigquery_client.query(query, job_config=job_config)
#             logging.info(f"source_runids_from_ytap job-id: {query_job.job_id}")
#             ytap_df = (
#                 query_job.to_arrow(bqstorage_client=settings.bigquery_storage_client)
#                 .to_pandas()
#                 .astype(
#                     {
#                         "RUN_ID": "Int64",
#                         "START_TIME": "datetime64[ns]",
#                         "GCS_PATH": "string",
#                     }
#                 )
#             )

#         else:

#             logging.info("Repeated Run Ids found in input")
#             return "data", (
#                 gdw_df,
#                 pd.DataFrame({"RUN_ID": [], "START_TIME": [], "GCS_PATH": []}).astype(
#                     {"RUN_ID": "Int64", "START_TIME": "datetime64[ns]"}
#                 ),
#             )

#         return "data", (gdw_df, ytap_df)

#     else:

#         return "data", (
#             gdw_df,
#             pd.DataFrame({"RUN_ID": [], "START_TIME": [], "GCS_PATH": []}).astype(
#                 {"RUN_ID": "Int64", "START_TIME": "datetime64[ns]"}
#             ),
#         )


# def infer_smartband_alg_version(
#     fab_name: str,
#     start_time_list: list,
#     run_id_list,
#     tool_name: str,
#     tool_id: str,
#     sensor_name: str,
# ) -> tuple:
#     """
#     Thread-blocking BQ query task
#     to map run-ids inferenced
#     using SmartBand algorithm
#     to the version
#     """

#     run_id_list_integer = [int(x) for x in run_id_list]
#     try:
#         start_time_list_date = [
#             datetime.strptime(x, "%Y-%m-%d %H:%M:%S") for x in start_time_list
#         ]
#     except:
#         start_time_list_date = [
#             datetime.strptime(x, "%Y-%m-%dT%H:%M:%S") for x in start_time_list
#         ]
#     min_date = min(start_time_list_date)
#     max_date = max(start_time_list_date)

#     query = """
#     SELECT
#         ALGORITHM_NAME,
#         ALGORITHM_VERSION,
#         ALGORITHM_PATH,
#         ALGO_MODULE_IMPORT_STR,
#         UPDATE_TIME AS ALGO_START_TIME,
#         LEAD(UPDATE_TIME) OVER (ORDER BY UPDATE_TIME) AS ALGO_END_TIME
#     FROM
#         `gdw-prod-fdis.fab_16_trace_analytics.ALGORITHM`
#     WHERE
#         ALGORITHM_NAME = 'SmartBand'
#     ORDER BY 
#         ALGO_START_TIME ASC
#     """.format(
#         fab_name=Fab(fab_name).get_fab_name("fab_"),
#         tool_id=tool_id,
#         tool_name=tool_name,
#         min_start_time=min_date.date(),
#         max_start_time=max_date.date(),
#         run_id_list_integer=run_id_list_integer,
#         run_id_list=run_id_list,
#         database=settings.database,
#         sensor_name=sensor_name,
#     )

#     # Always use cache for smartband alg version fetch
#     # job_config = bigquery.QueryJobConfig(use_query_cache=settings.use_query_cache)
#     job_config = bigquery.QueryJobConfig(use_query_cache=True)
#     query_job = settings.bigquery_client.query(query, job_config=job_config)
#     logging.info(f"infer_smartband_alg_version job-id: {query_job.job_id}")
#     # return "alg", spanwrap(tracer=tracer)(query_job.to_dataframe)(
#     #     bqstorage_client=settings.bigquery_storage_client
#     # )
#     return (
#         "alg",
#         query_job.to_arrow(
#             bqstorage_client=settings.bigquery_storage_client
#         ).to_pandas(),
#     )


# # TODO: Retrofit src.utils.helpers.retrieval for improved efficiency
# def pre_processing(
#     sources_df_list: Iterable, request_json: dict, ctx: Context = nullcontext
# ) -> Iterable:
#     # Index sources
#     sources_indexing = {source[0]: idx for idx, source in enumerate(sources_df_list)}

#     # Prepare requests from sources & prepare list of destinations
#     source_base_df = sources_df_list[sources_indexing["alg"]][1]
#     source_base_df["ALGO_END_TIME"] = source_base_df["ALGO_END_TIME"].fillna(
#         pd.to_datetime(
#             pd.Timestamp.now(tz=Fab(request_json["fab_name"]).get_timezone()).replace(
#                 tzinfo=None
#             )
#         )
#     )
#     source_base_df.index = pd.IntervalIndex.from_arrays(
#         source_base_df["ALGO_START_TIME"],
#         source_base_df["ALGO_END_TIME"],
#         closed="left",
#     )

#     source_from_gdw, source_from_ytap = sources_df_list[sources_indexing["data"]][1]

#     # TODO: Optimize
#     if "furnace" in request_json["tool_name"].lower():
#         try:
#             load_limit = (
#                 abs(int(current_app.config["FURNACE_LL"]))
#                 if isinstance(current_app.config["FURNACE_LL"], Real)
#                 else max(source_from_gdw.shape[0], source_from_ytap.shape[0])
#             )
#             gdw_lpi = (
#                 abs(current_app.config["GDW_FURNACE_LPI"])
#                 if isinstance(current_app.config["GDW_FURNACE_LPI"], Real)
#                 else source_from_gdw.shape[0]
#             )
#             ytap_lpi = (
#                 abs(current_app.config["YTAP_FURNACE_LPI"])
#                 if isinstance(current_app.config["YTAP_FURNACE_LPI"], Real)
#                 else source_from_ytap.shape[0]
#             )
#         except:
#             # keyerror but we dont restrict to it
#             gdw_lpi = max(
#                 source_from_gdw.shape[0], 1
#             )  # Avoid division by zero or atleast one run per instance
#             ytap_lpi = max(
#                 source_from_ytap.shape[0], 1
#             )  # Avoid division by zero or atleast one run per instance
#             load_limit = max(source_from_gdw.shape[0], source_from_ytap.shape[0])
#     else:
#         try:
#             load_limit = (
#                 abs(int(current_app.config["NON_FURNACE_LL"]))
#                 if isinstance(current_app.config["NON_FURNACE_LL"], Real)
#                 else max(source_from_gdw.shape[0], source_from_ytap.shape[0])
#             )
#             gdw_lpi = (
#                 abs(current_app.config["GDW_NON_FURNACE_LPI"])
#                 if isinstance(current_app.config["GDW_NON_FURNACE_LPI"], Real)
#                 else source_from_gdw.shape[0]
#             )
#             ytap_lpi = (
#                 abs(current_app.config["YTAP_NON_FURNACE_LPI"])
#                 if isinstance(current_app.config["YTAP_NON_FURNACE_LPI"], Real)
#                 else source_from_ytap.shape[0]
#             )
#         except:
#             # keyerror but we dont restrict to it
#             gdw_lpi = max(
#                 source_from_gdw.shape[0], 1
#             )  # Avoid division by zero or atleast one run per instance
#             ytap_lpi = max(
#                 source_from_ytap.shape[0], 1
#             )  # Avoid division by zero or atleast one run per instance
#             load_limit = max(source_from_gdw.shape[0], source_from_ytap.shape[0])

#     source_from_gdw = source_from_gdw.iloc[:load_limit, :]
#     source_from_ytap = source_from_ytap.iloc[
#         : max(load_limit - source_from_gdw.shape[0], 0), :
#     ]

#     if source_from_gdw.shape[0] > 0:

#         source_from_gdw = pd.concat(
#             [
#                 source_from_gdw,
#                 source_from_gdw["START_TIME"].apply(
#                     lambda x: source_base_df.iloc[source_base_df.index.get_loc(x)].loc[
#                         [
#                             "ALGORITHM_NAME",
#                             "ALGORITHM_VERSION",
#                             "ALGORITHM_PATH",
#                             "ALGO_MODULE_IMPORT_STR",
#                         ]
#                     ]
#                 ),
#             ],
#             axis=1,
#         )
#         destinations = (
#             source_from_gdw.groupby(
#                 [
#                     "ALGORITHM_VERSION",
#                     source_from_gdw.index // gdw_lpi,
#                 ],  # Post-pone load chunking to later
#                 # ["ALGORITHM_VERSION"],
#                 group_keys=True,
#             )
#             .apply(
#                 lambda x: partial(
#                     send_http_request_multiple_gdw_runs, x, request_json, ctx
#                 )
#             )
#             .tolist()
#         )

#     else:

#         destinations = []

#     if source_from_ytap.shape[0] > 0:

#         source_from_ytap = pd.concat(
#             [
#                 source_from_ytap,
#                 source_from_ytap["START_TIME"].apply(
#                     lambda x: source_base_df.iloc[source_base_df.index.get_loc(x)].loc[
#                         [
#                             "ALGORITHM_NAME",
#                             "ALGORITHM_VERSION",
#                             "ALGORITHM_PATH",
#                             "ALGO_MODULE_IMPORT_STR",
#                         ]
#                     ]
#                 ),
#             ],
#             axis=1,
#         )
#         destinations.extend(
#             source_from_ytap.groupby(
#                 [
#                     "ALGORITHM_VERSION",
#                     source_from_ytap.index // ytap_lpi,
#                 ],  # Post-pone load chunking to later
#                 # ["ALGORITHM_VERSION"],
#                 group_keys=True,
#             )
#             .apply(lambda x: partial(send_http_request_ytap_runs, x, request_json, ctx))
#             .tolist()
#         )

#     return destinations


# def post_processing(results_json_list: Iterable) -> str:
#     return pd.concat(
#         [pd.DataFrame(result_json) for result_json in results_json_list],
#     ).to_json(orient="records")
